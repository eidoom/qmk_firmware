# [eidoom/qmk_firmware](https://gitlab.com/eidoom/qmk_firmware)

## Install repo

```bash
git clone --recurse-submodules git@gitlab.com:eidoom/qmk_firmware.git <location>
cd <location>
git remote add upstream git@github.com:qmk/qmk_firmware.git
```

## Update repo from upstream

```bash
python3 -m pip install --user --upgrade qmk
qmk clean -a
git pull upstream master
qmk git-submodule
sudo cp <location>/util/udev/50-qmk.rules /etc/udev/rules.d/
```

## Install QMK

```bash
sudo dnf install hidapi avr-gcc avr-libc dfu-programmer dfu-util  # Fedora 34
python3 -m pip install --user qmk
qmk setup -H <location>
sudo cp <location>/util/udev/50-qmk.rules /etc/udev/rules.d/
```

## Troubleshooting

```bash
qmk doctor
```

## Changes: new layouts

* [iris](https://gitlab.com/eidoom/iris-keymap) @ `keyboards/keebio/iris/keymaps/eidoom`
* [lily58](https://gitlab.com/eidoom/lily58-keymap) @ `keyboards/lily58/keymaps/eidoom`
